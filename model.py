class Model:
    _avaliacoesUsuario = {'Ana': 
		{'Freddy x Jason': 2.5, 
		 'O Ultimato Bourne': 3.5,
		 'Star Trek': 3.0, 
		 'Exterminador do Futuro': 3.5, 
		 'Norbit': 2.5, 
		 'Star Wars': 3.0},
	 
	  'Marcos': 
		{'Freddy x Jason': 3.0, 
		 'O Ultimato Bourne': 3.5, 
		 'Star Trek': 1.5, 
		 'Exterminador do Futuro': 5.0, 
		 'Star Wars': 3.0, 
		 'Norbit': 3.5}, 

	  'Pedro': 
	    {'Freddy x Jason': 2.5, 
		 'O Ultimato Bourne': 3.0,
		 'Exterminador do Futuro': 3.5, 
		 'Star Wars': 4.0},
			 
	  'Claudia': 
		{'O Ultimato Bourne': 3.5, 
		 'Star Trek': 3.0,
		 'Star Wars': 4.5, 
		 'Exterminador do Futuro': 4.0, 
		 'Norbit': 2.5},
				 
	  'Adriano': 
		{'Freddy x Jason': 3.0, 
		 'O Ultimato Bourne': 4.0, 
		 'Star Trek': 2.0, 
		 'Exterminador do Futuro': 3.0, 
		 'Star Wars': 3.0,
		 'Norbit': 2.0}, 

	  'Janaina': 
	     {'Freddy x Jason': 3.0, 
	      'O Ultimato Bourne': 4.0,
	      'Star Wars': 3.0, 
	      'Exterminador do Futuro': 5.0, 
	      'Norbit': 3.5},
			  
	  'Leonardo': 
	    {'O Ultimato Bourne':4.5,
             'Norbit':1.0,
	     'Exterminador do Futuro':4.0}
    }

    _avaliacoesFilme = {'Freddy x Jason': 
		{'Ana': 2.5, 
		 'Marcos:': 3.0 ,
		 'Pedro': 2.5, 
		 'Adriano': 3.0, 
		 'Janaina': 3.0 },
	 
	 'O Ultimato Bourne': 
		{'Ana': 3.5, 
		 'Marcos': 3.5,
		 'Pedro': 3.0, 
		 'Claudia': 3.5, 
		 'Adriano': 4.0, 
		 'Janaina': 4.0,
		 'Leonardo': 4.5 },
				 
	 'Star Trek': 
		{'Ana': 3.0, 
		 'Marcos:': 1.5,
		 'Claudia': 3.0, 
		 'Adriano': 2.0 },
	
	 'Exterminador do Futuro': 
		{'Ana': 3.5, 
		 'Marcos:': 5.0 ,
		 'Pedro': 3.5, 
		 'Claudia': 4.0, 
		 'Adriano': 3.0, 
		 'Janaina': 5.0,
		 'Leonardo': 4.0},
				 
	 'Norbit': 
		{'Ana': 2.5, 
		 'Marcos:': 3.0 ,
		 'Claudia': 2.5, 
		 'Adriano': 2.0, 
		 'Janaina': 3.5,
		 'Leonardo': 1.0},
				 
	 'Star Wars': 
		{'Ana': 3.0, 
		 'Marcos:': 3.5,
		 'Pedro': 4.0, 
		 'Claudia': 4.5, 
		 'Adriano': 3.0, 
		 'Janaina': 3.0}
    }
    
    def getAvaliacoesUsuario(self):
        return self._avaliacoesUsuario
    
    def getAvaliacoesFilme(self):
        return self._avaliacoesFilme
    
    def getDbMovieLensUsuario(self,path = 'database-movieLens/ml-100k'):
        filmes = {}
        for linha in open(path + '/u.item', encoding='"ISO-8859-1"'):
            (id, titulo) = linha.split('|')[0:2]
            filmes[id] = titulo
        base = {}
        for linha in open(path + '/u.data', encoding='"ISO-8859-1"'):
            (usuario, idfilme, nota, tempo) = linha.split('\t')
            base.setdefault(usuario, {})
            base[usuario][filmes[idfilme]] = float(nota)
        return base
    
    def getDbMovieLensFilme(self,path = 'database-movieLens/ml-100k'):
        usuarios = {}
        for linha in open(path + '/u.user', encoding='"ISO-8859-1"'):
            (id, name) = linha.split('|')[0:2]
            usuarios[id] = name
        
        filmes = {}
        for linha in open(path + '/u.item', encoding='"ISO-8859-1"'):
            (id, titulo) = linha.split('|')[0:2]
            filmes[id] = titulo

        base = {}
        for linha in open(path + '/u.data', encoding='"ISO-8859-1"'):
            (idusuario, idfilme, nota, tempo) = linha.split('\t')
            base.setdefault(filmes[idfilme], {})
            base[filmes[idfilme]][usuarios[idusuario]] = float(nota)
        return base