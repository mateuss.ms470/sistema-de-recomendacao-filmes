from model import *
from analyzes import *
from beautifultable import BeautifulTable

class Controller:
    filmesSimilaresAvaliacoes = None    # Guardar variável em banco de dados pois o cálculo para obetr seu valor é demorado
    filmesSimilaresMvLens = None        # Guardar variável em banco de dados pois o cálculo para obetr seu valor é demorado

    def __init__(self):
        self.oModel = Model()
        self.oAnalyzes = Analyzes()

    def calcularSemelhancaUsuarios(self,base):
        aTables = []
        for usuario in base:
            oTable = BeautifulTable()
            oTable.columns.header = ["Similaridade com "+usuario]
            aHeaderRowsAux = []
            for aSimilaridade in self.oAnalyzes.getSimilares(base,usuario):
                aHeaderRowsAux += [aSimilaridade[1]]
                oTable.rows.append([''+str(round(aSimilaridade[0]*100,3))+'%'])
            oTable.rows.header = aHeaderRowsAux
            if oTable.rows.__len__() != 0:
                aTables += [oTable]
        return aTables
    
    def calcularRecomendacoesPorUsuario(self,base):
        aTables = []
        for usuario in base:
            oTable = BeautifulTable()
            oTable.columns.header = ["Recomendações para "+usuario]
            aHeaderRowsAux = []
            for aRecomendacoes in self.oAnalyzes.getRecomendacoesUsuario(base,usuario):
                aHeaderRowsAux += [aRecomendacoes[1]]
                oTable.rows.append([aRecomendacoes[0]])
            oTable.rows.header = aHeaderRowsAux
            if oTable.rows.__len__() != 0:
                aTables += [oTable]

        return aTables
    
    def calcularSemelhancaFilmes(self,base):
        aTables = []
        for filme in base:
            oTable = BeautifulTable()
            oTable.columns.header = ["Similaridade com o filme "+filme]
            aHeaderRowsAux = []
            for aSimilaridade in self.oAnalyzes.getSimilares(base,filme):
                aHeaderRowsAux += [aSimilaridade[1]]
                oTable.rows.append([''+str(round(aSimilaridade[0]*100,3))+'%'])
            oTable.rows.header = aHeaderRowsAux
            if oTable.rows.__len__() != 0:
                aTables += [oTable]
        return aTables
    
    def calcularRecomendacoesPorFilmes(self,base,similaridadeFilmes):
        aTables = []
        for usuario in base:
            oTable = BeautifulTable()
            oTable.columns.header = ["Recomendações para "+usuario]
            aHeaderRowsAux = []
            for aRecomendacoes in self.oAnalyzes.getRecomendacoesFilmes(base,similaridadeFilmes,usuario):
                aHeaderRowsAux += [aRecomendacoes[1]]
                oTable.rows.append([aRecomendacoes[0]])
            oTable.rows.header = aHeaderRowsAux
            if oTable.rows.__len__() != 0:
                aTables += [oTable]
        return aTables

    def execute(self): # Para utilizar base do Movie Lens basta descomentar as declareçẽos
        baseAvaliacoesUsuarios  = self.oModel.getAvaliacoesUsuario()
        # baseMvLensUsuarios      = self.oModel.getDbMovieLensUsuario()
        baseAvaliacoesFilmes    = self.oModel.getAvaliacoesFilme()
        # baseMvLensFilmes        = self.oModel.getDbMovieLensFilme()
        
        if self.filmesSimilaresAvaliacoes == None:
            self.filmesSimilaresAvaliacoes = self.oAnalyzes.getFilmesSimilares(baseAvaliacoesFilmes)
        # if self.filmesSimilaresMvLens == None:
        #     self.filmesSimilaresMvLens = self.oAnalyzes.getFilmesSimilares(baseMvLensFilmes)

        print('\nSimilaridade avaliações usuário'.upper())
        for similaridadeUsuarioAv in self.calcularSemelhancaUsuarios(baseAvaliacoesUsuarios):
            print(similaridadeUsuarioAv)
        print(50*'#')

        # print('\nSimilaridadeUsuarioMv avaliações usuário Movie Lens'.upper())
        # for similaridade in self.calcularSemelhancaUsuarios(baseMvLensUsuarios):
        #     print(SimilaridadeUsuarioMv)
        # print(50*'#')

        print('\nRecomendação de filmes via similaridade entre os usuários'.upper())
        for recomendacaoUsuarioAv in self.calcularRecomendacoesPorUsuario(baseAvaliacoesUsuarios):
            print(recomendacaoUsuarioAv)
        print(50*'#')

        # print('\nRecomendação de filmes via similaridade entre os usuários Movie Lens'.upper())
        # for recomendacaoUsuarioMv in self.calcularRecomendacoesPorUsuario(baseMvLensUsuarios):
        #     print(recomendacaoUsuarioMv)
        # print(50*'#')
        
        print('\nSimilaridade avaliações filmes'.upper())
        for similaridadeFilmesAv in self.calcularSemelhancaFilmes(baseAvaliacoesFilmes):
            print(similaridadeFilmesAv)
        print(50*'#')

        # print('\nSimilaridade avaliações filmes Movie Lens'.upper())
        # for similaridadeFilmesMv in self.calcularSemelhancaFilmes(baseMvLensFilmes):
        #     print(similaridadeFilmesMv)
        # print(50*'#')

        print('\nRecomendação de filmes via similaridade entre os filmes vistos pelo usuário'.upper())
        for recomendacaoFilmesAv in self.calcularRecomendacoesPorFilmes(baseAvaliacoesUsuarios,self.filmesSimilaresAvaliacoes):
            print(recomendacaoFilmesAv)
        print(50*'#')

        # print('\nRecomendação de filmes via similaridade entre os filmes vistos pelo usuário Movie Lens'.upper())
        # for recomendacaoFilmesMv in self.calcularRecomendacoesPorFilmes(baseMvLensUsuarios,self.filmesSimilaresMvLens):
        #     print(recomendacaoFilmesMv)
        # print(50*'#')
        
        print('\nPara utilizar a base de dados do Movie Lens basta descomentar as chamadas no método execute da classe Controller.')
        print('OBS.: O processamento ficará mais demorado por causa da quantidade de dados.\n')