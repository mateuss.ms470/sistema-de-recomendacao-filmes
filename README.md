# Sistema de Recomendação - Filmes



## 1.Instalação
Para instalar o projeto realize o clone e instale a biblioteca beautifultable (para apresentação dos dados):
```
git clone https://gitlab.com/mateuss.ms470/sistema-de-recomendacao-filmes.git
pip install beautifultable
```
## 2.Similaridade
<p>Realizarei a análise da similaridade com base no cálculo da <strong>Distância Euclidiana</strong> utilizando como parâmetro as avaliações dos filmes. Assim será possível de calcular a similaridade entre usuários e entre filmes. Neste projeto não utilizei a filtro de categorias, por se tratar de um exemplo simples.</p>
<br>

![terminal](https://fabiobaldini.com.br/wp-content/uploads/2020/05/KNN_DEuclidiana.jpg)

## 3.Recomendação baseada em usuários
<p>Primeiro calcula-se a similaridade do usuário alvo com todos os demais usuários, então multiplicamos a similaridade com as avaliações dadas aos filmes. Assim trataremos filme por filme a ser recomendado pegando os respectivos resultados da multiplicação de todos os usuários; caso o usuário tenha avaliado determinado filme. E faz-se o cálculo da <strong>Média Ponderada</strong> utilizando o somátorio da multiplicação da similaridade com as avaliações divido pelo somatório das similaridades; atentando-se para não incluir a similaridade que não haja avaliação em determinado filme. Com isso obteremos as possíveis avaliações que o usuário alvo daria aos filmes que ainda não assistiu.</p>
<br>

![terminal](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQLD2xpySsXTRiGsA1YvuV-VfdjVOuPWKAIbQ&usqp=CAU)

### 3.1.Problemas da recomendação baseada em usuários
<p>Pessoas que não tenham feito avaliações sobre filmes que podem ser de seu interesse.</p>

<p>Pessoas que tenham gostado de filmes mal avaliados por todos os demais.</p>

## 4.Recomendação baseada em filmes avaliados pelo usuário
<p>Inicialmente calculamos a similaridade dos filmes avaliados com os filmes não avaliados, então multiplicamos a similaridade com as avaliações dadas aos filmes que foram avaliados. Com isso tratamos filme por filme a ser recomendado obtendo os reultados da multiplicação de todos os filmes. Logo realizamos o cáculo da <strong>Média Ponderada</strong> entre o somatório da multiplicação da similaridade com as avaliações pelo somatório das similaridades. Assim obteremos as possíveis avaliações que o usuário alvo daria aos filmes que ainda não assistiu.</p>

### 4.1.Problemas da recomendação baseada em filmes avaliados pelo usuário
<p>Necessita de muitos dados.</p>

<p>Os cálculos são mais demorados.</p>

## 5.Recomendação baseada em usuários x Recomendação baseada em filmes avaliados pelo usuário

### 5.1.Quando utilizar Recomendação baseada em usuários
<p>Situações simples.</p>

<p>Cálculo mais rápido.</p>

<p>Quando o conjunto de dados for pequeno e denso.</p>

### 5.2.Quando utilizar Recomendação baseada em filmes avaliados pelo usuário
<p>Situções que exigem uma análise mais cuidadosa.</p>

<p>No cenário em que há um banco de dados para guardar os pré-cálculos da similaridade, devido a sua demora; assim atualizando os dados quando necessário.</p>

<p>Quando o conjunto de dados for grande, podendo ser denso ou esparso.</p>

## 6.Movie Lens
<p>Fora implemetado exemplos utilizando a base de dados da [movie lens](https://grouplens.org/datasets/movielens/), não utilizei todos os conjuntos, mas apenas alguns que eram o suficiente para realizar a análise. Para utilizar tais exemplos basta descomentar os métodos no arquivo controller.py</p>
