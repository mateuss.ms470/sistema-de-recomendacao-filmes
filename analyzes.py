from math import sqrt

class Analyzes:

    def semelhanca(self,base, item1, item2):
        semelhantes = {}
        for filme in base[item1]:
            if filme in base[item2]: 
                semelhantes[filme] = 1

        if len(semelhantes) == 0: return 0

        soma = sum([pow(base[item1][filme] - base[item2][filme], 2) 
            for filme in semelhantes])
        return 1/(1 + sqrt(soma))
    
    def getSimilares(self, base, item):
        similaridade = [(self.semelhanca(base, item, outro), outro)
                        for outro in base if outro != item]
        similaridade.sort(reverse=True)
        return similaridade  
    
    def getRecomendacoesUsuario(self,base, usuario):
        totais={}
        somaSimilaridade={}
        for outro in base:
            if outro == usuario: continue
            similaridade = self.semelhanca(base, usuario, outro)
    
            if similaridade <= 0: continue
    
            for item in base[outro]:
                if item not in base[usuario]:
                    totais.setdefault(item, 0)
                    totais[item] += base[outro][item] * similaridade
                    somaSimilaridade.setdefault(item, 0)
                    somaSimilaridade[item] += similaridade
        rankings=[(total / somaSimilaridade[item], item) for item, total in totais.items()]
        rankings.sort(reverse=True)
        return rankings

    def getFilmesSimilares(self,base):
        result = {}
        for filme in base:
            notas = self.getSimilares(base, filme)
            result[filme] = notas
        return result

    def getRecomendacoesFilmes(self,baseUsuario, similaridadeFilmes, usuario):
        notasUsuario = baseUsuario[usuario]
        notas={}
        totalSimilaridade={}
        for (filme, nota) in notasUsuario.items():
            for (similaridade, filme2) in similaridadeFilmes[filme]:
                if filme2 in notasUsuario: continue
                notas.setdefault(filme2, 0)
                notas[filme2] += similaridade * nota
                totalSimilaridade.setdefault(filme2,0)
                totalSimilaridade[filme2] += similaridade
        rankings=[(score/totalSimilaridade[filme], filme) for filme, score in notas.items()]
        rankings.sort(reverse=True)
        return rankings